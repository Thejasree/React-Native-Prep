import React, { Component } from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    View
} from 'react-native';
import { SearchBar } from 'react-native-elements';
export default class Search extends  Component{
    render(){
        return(
            <View>
            <SearchBar
                lightTheme
                onChangeText={someMethod}
                placeholder='Type Here...' />
            </View>
        );
    }
}