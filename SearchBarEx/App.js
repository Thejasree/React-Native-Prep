/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    View,
    AppRegistry
} from 'react-native';

import Search from './Search';

export default class App extends Component<> {
    render() {
        return (
            <View style={styles.container}>
              <Search />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
    },

});


AppRegistry.registerComponent('SearchBarEx', () => App);