import React from 'react';
import { StyleSheet, Text, View, AppRegistry , Button} from 'react-native';
import { StackNavigator } from 'react-navigation';
export default class HomeScreen extends React.Component {
    static navigationOptions = {
        title: 'Welcome'
    };
    render() {
        const { navigate } = this.props.navigation;
        return(
            <View>
            <Text>Hello, this is Home page!</Text>

        <Button
                onPress={() => navigate('Profile',{user: 'John'})}
                title="profile!"
                />
            </View>
        );
    }
}