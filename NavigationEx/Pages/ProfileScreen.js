import React from 'react';
import { StyleSheet, Text, View, AppRegistry } from 'react-native';

export default class ProfileScreen extends React.Component {
    // Nav options can be defined as a function of the screen's props:
    static navigationOptions = ({ navigation }) => ({
        title: `profile of ${navigation.state.params.user}`,
    });
    render() {
        // The screen's current route is passed in to `props.navigation.state`:
        const { params } = this.props.navigation.state;
        return (
            <View>
                <Text>This is the profile of  {params.user}</Text>
            </View>
        );
    }
}